import neuropsydia as n
import UserTestChoice


def reaction_B():
    n.newpage()
    n.instructions("Test is under development. Please return later :)\n\nPress q if you want to exit the"
                   " program.\n\nPress other keys if you want to return to the menu")

    response, RT = n.core.response()

    if response == "q":  # Maybe we need to define the key "q" in core.py
        n.close()
    else:
        UserTestChoice.user_choice()
