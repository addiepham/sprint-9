"""
This program enables users to perform Stroop Test, which is a neuropsychological test that requires
the users to demonstrate when there is a mismatch between the name of a color
(e.g., "blue", "green", or "red") and the color it is printed on
(i.e., the word "red" printed in blue ink instead of red ink).

More information regarding the Stroop Test can be found here: https://en.wikipedia.org/wiki/Stroop_effect
"""

import neuropsydia as n
import numpy as np  # Load numpy
import pandas as pd  # Load pandas
import winsound


def stroop_test():
    n.newpage()
    # Initialize data storage

    data = {"Stimulus": [],
            "Stimulus_Color": [],
            "Answer": [],
            "RT": [],
            "Condition": [],
            "Correct": []}

    # ==============================================================================
    # Part 1: Neutral stimulus, denoted by "XXXX" word
    # ==============================================================================

    n.instructions("Press LEFT when RED, DOWN when GREEN and RIGHT when BLUE\n\nPress SPACE if you want to exit"
                   "\n\nPress ENTER to continue")
    n.newpage()
    n.write("This is a trial Stroop Test to help you get familiarized with the test.", y=1.5)
    n.write("\n\nThe test will commence after the sound BEEP", y=0.5)
    n.refresh()
    n.time.wait(3500)
    winsound.Beep(500, 1000)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms

    n_trials = 10
    for trial in range(n_trials):
        n.newpage("grey")  # Neutral grey background
        n.write("+")  # Fixation cross
        n.refresh()  # Display it
        n.time.wait(250)  # Wait 250 ms

        stim_color = np.random.choice(["raw_red", "raw_green", "raw_blue"])  # Choose a color
        stim = "XXXX"
        n.newpage("grey")  # Neutral grey background
        n.write(stim, style="bold", color=stim_color, size=3)  # Load the stimulus
        n.refresh()  # Display it
        answer, RT = n.response()  # Record response and response time
        if answer == "ESCAPE":  # Enable experiment quit by pressing escape
            quit()

        # Append info
        data["Stimulus"].append(stim)
        data["Stimulus_Color"].append(stim_color)
        data["Answer"].append(answer)
        data["RT"].append(RT)
        data["Condition"].append("Neutral")

        # Categorize the response
        if answer == "LEFT" and stim_color == "raw_red":
            data["Correct"].append(1)
        elif answer == "DOWN" and stim_color == "raw_green":
            data["Correct"].append(1)
        elif answer == "RIGHT" and stim_color == "raw_blue":
            data["Correct"].append(1)
        else:
            data["Correct"].append(0)

    # ==============================================================================
    # Part 2: Real Stroop Test
    # ==============================================================================

    n.instructions("Press LEFT when RED, DOWN when GREEN and RIGHT when BLUE.\n\nPress SPACE if you want to exit"
                   "\n\nPress ENTER to continue")
    n.newpage()
    n.write("This is the real Stroop Test. The stimulus will be a color name, not XXXX.", y=1.5)
    n.write("\n\nThe test will commence after the sound BEEP", y=0.5)
    n.refresh()
    n.time.wait(3500)
    winsound.Beep(500, 1000)  # Play sound beep for 500Hz for 1 second, aka. 1000 ms

    n_trials = 10
    for trial in range(n_trials):
        n.newpage("grey")  # Neutral grey background
        n.write("+")  # Fixation cross
        n.refresh()  # Display it
        n.time.wait(250)  # Wait 250 ms

        stim_color = np.random.choice(["raw_red", "raw_green", "raw_blue"])  # Choose a color
        stim = np.random.choice(["RED", "GREEN", "BLUE"])
        n.newpage("grey")  # Neutral grey background
        n.write(stim, style="bold", color=stim_color, size=3)  # Load the stimulus
        n.refresh()  # Display it
        answer, RT = n.response()  # Record response and response time
        if answer == "ESCAPE":  # Enable experiment quit by pressing escape
            quit()

        # Append trial info to
        data["Stimulus"].append(stim)
        data["Stimulus_Color"].append(stim_color)
        data["Answer"].append(answer)
        data["RT"].append(RT)

        # Categorize the condition
        if stim == "RED" and stim_color == "raw_red":
            data["Condition"].append("Congruent")
        elif stim == "GREEN" and stim_color == "raw_green":
            data["Condition"].append("Congruent")
        elif stim == "BLUE" and stim_color == "raw_blue":
            data["Condition"].append("Congruent")
        else:
            data["Condition"].append("Incongruent")

        # Categorize the response
        if answer == "LEFT" and stim_color == "raw_red":
            data["Correct"].append(1)
        elif answer == "DOWN" and stim_color == "raw_green":
            data["Correct"].append(1)
        elif answer == "RIGHT" and stim_color == "raw_blue":
            data["Correct"].append(1)
        else:
            data["Correct"].append(0)

    df = pd.DataFrame.from_dict(data)  # Convert dict to a dataframe
    df.to_csv("data.csv")  # Save data


