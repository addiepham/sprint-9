"""
Allow users to choose the test they want to perform
 There are currently two (02) tests for this program (still under development):
 - Reaction Test A: using keyboard to perform (Sprint 9)
 - Reaction Best: using Lithic sensor to perform (Sprint 10 onwards)
"""

import neuropsydia as n
import ReactionTestA
import ReactionTestB


def user_choice():
    n.newpage()  # Start a new page

    n.write("What test would you like\nto perform today?", y=5)
    response = n.choice(["Reaction Test A", "Reaction Test B"], y=0,
                        height=-4, boxes_edge_size=2, help_list=["Use your KEYBOARD to perform the Reaction Test", "Use your SENSOR to perform the Reaction Test"])

    if response == "Reaction Test A":
        ReactionTestA.reaction_A()  # Start Reaction Test A: using keyboard

    if response == "Reaction Test B":
        ReactionTestB.reaction_B()  # Start Reaction Test B: using sensor. Currently under development
