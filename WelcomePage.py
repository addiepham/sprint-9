"""  This project uses Neuropsydia: A Python Module for Creating Experiments, Tasks and Questionnaires
The link to Neuropsydia can be found here: https://github.com/neuropsychology/Neuropsydia.py
"""

import neuropsydia as n
from time import sleep
# import StroopTest
import UserTestChoice


""" 
A few notes about the error: Attribute Error: "Event" has no object "key"
Maybe it is because in core.py, the keys have not been fully defined. 
"""


def main():
    n.start()  # Start the program
    n.write("Welcome!", style="title")
    n.write("Thanks for visiting :) Here's a cat.", y=4)
    n.image("cat1.jpg", size=10, y=-3.5)
    n.refresh()
    n.time.wait(3500)

    UserTestChoice.user_choice()  # Allow users to choose the test they want to perform

    n.close()  # Close the program


if __name__ == '__main__':
    main()


